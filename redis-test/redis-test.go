package main

import (
	"fmt"

	"github.com/go-redis/redis"
)

func main() {

	/**
	* =============================================
	* 连接redis
	* =============================================*/
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})

	/**
	* =============================================
	* String
	* =============================================*/
	// 设置值
	client.Set("test", "123", 0)
	// 取值
	val, err := client.Get("test").Result()
	if err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(val)
	}

	/**
	* =============================================
	* List
	* =============================================*/
	// 入队列
	client.LPush("test-list", "a")
	// 出队列
	valList, errList := client.LPop("test-list").Result()
	if errList != nil {
		fmt.Println(errList.Error())
	} else {
		fmt.Println(valList)
	}

	/**
	* =============================================
	* Set
	* =============================================*/
	// 添加元素
	client.SAdd("test-set", "set-a")
	// 返回元素
	valSet := client.SPop("test-set").Val()
	fmt.Println(valSet)

	/**
	* =============================================
	* zSet
	* =============================================*/
	// 添加元素
	m := redis.Z{
		Score:  5,
		Member: "c",
	}
	m1 := redis.Z{
		Score:  6,
		Member: "d",
	}
	client.ZAdd("teat-zSet", m, m1)
	// 取出元素
	valZSet := client.ZRange("teat-zSet", 0, 10).String()
	fmt.Println(valZSet)

	/**
	* =============================================
	* Hash
	* =============================================*/
	// 设置元素（单个）
	client.HSet("test-hash", "key", "value")
	// 取出元素（单个）
	valHash := client.HGet("test-hash", "key").Val()
	fmt.Println(valHash)
	//设置元素（多个）
	_field := map[string]interface{}{
		"key1": "value1",
		"key2": "value2",
	}
	client.HMSet("test-hash", _field)
	// 取出元素（多个）
	valHashMany := client.HMGet("test-hash", "key1", "key2").Val()
	for k, v := range valHashMany {
		fmt.Println(k)
		fmt.Println(v)
	}
}
