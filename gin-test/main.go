package main

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"os"

	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"github.com/gin-gonic/gin"
)

func main() {

	r := gin.Default()

	// 使用自定义中间件
	r.Use(ResponseHeaders())

	// gin hello world
	r.GET("/gin", func(c *gin.Context) {
		// 返回json数据
		c.JSON(200, gin.H{
			"message": "hello world",
		})
	})

	// 处理post请求
	r.POST("/login", func(c *gin.Context) {
		// 获取POST参数
		id := c.Query("id")                          // 获取参数id
		name := c.DefaultPostForm("name", "default") // 获取参数name，没获取到则使用默认值
		c.JSON(200, gin.H{
			"id":   id,
			"name": name,
		})
	})

	// 加载模版文件
	r.LoadHTMLGlob("view/index.html")

	// 访问view/index.html页面
	r.GET("/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", nil)
	})

	// 单文件上传
	r.POST("/uploadfile", func(c *gin.Context) {

		f, err := c.FormFile("f1")
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": err,
			})
			return
		}

		// 保存在指定路径
		c.SaveUploadedFile(f, "upload/"+f.Filename)

		// 将文件上传到 阿里云 oss
		UploadAliyunOss(f)

		c.JSON(http.StatusOK, gin.H{
			"message": "OK",
		})

	})

	// 监听8080端口
	r.Run("0.0.0.0:8080")
}

// 自定义中间件
func ResponseHeaders() gin.HandlerFunc {
	return func(context *gin.Context) {
		// 自定义头部信息
		context.Header("Access-Control-Allow-Origin", "*")
		context.Header("Token", "FootMark")
		// 继续调用其他的内置中间件
		context.Next()
	}
}

// 将文件上传到 阿里云 oss
func UploadAliyunOss(file *multipart.FileHeader) {

	Endpoint := "https://oss-cn-guangzhou.aliyuncs.com" // 这里的是广州区，
	AccessKeyID := "yourAccessKeyID"                    //
	AccessKeySecret := "yourAccessKeySecret"            //

	client, err := oss.New(Endpoint, AccessKeyID, AccessKeySecret)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}

	// 指定bucket
	bucket, err := client.Bucket("yourBucket") // 根据自己的填写
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}

	src, err := file.Open()
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}
	defer src.Close()

	// 将文件流上传至test目录下
	path := "test/" + file.Filename
	err = bucket.PutObject(path, src)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-1)
	}

	fmt.Println("file upload success")
}
