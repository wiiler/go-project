module gin-test

go 1.16

require (
	github.com/aliyun/aliyun-oss-go-sdk v2.2.1+incompatible
	github.com/baiyubin/aliyun-sts-go-sdk v0.0.0-20180326062324-cfa1a18b161f // indirect
	github.com/gin-gonic/gin v1.7.7
	github.com/satori/go.uuid v1.2.0 // indirect
	golang.org/x/time v0.0.0-20220224211638-0e9765cccd65 // indirect
)
