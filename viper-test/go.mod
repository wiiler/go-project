module load-conf-test

go 1.16

require (
	github.com/spf13/viper v1.10.1
	github.com/stretchr/testify v1.7.1 // indirect
	gopkg.in/ini.v1 v1.66.4
)
