package api

import (
	"im-demo/application/model"
	"net/http"

	"github.com/gin-gonic/gin"
)

// UpdateSession 更新会话列表
func UpdateSession(context *gin.Context) {
	var update model.UpdateData
	if err := context.Bind(&update); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10301,
			"msg":  "update session fail," + err.Error(),
		})
		return
	}

	if err := model.UpdateSession(&update); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10301,
			"msg":  "update session fail," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "update session success",
	})
}

// GetSessionList 获取会话列表
func GetSessionList(context *gin.Context) {
	ownerId := context.Query("owner_id")
	data, err := model.GetOwnerSession(ownerId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10302,
			"msg":  "get owner session fail," + err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "get owner session success",
		"data": data,
	})
}

type DelData struct {
	OwnerId string
	OtherId string
}

// DelSession 删除会话列表
func DelSession(context *gin.Context) {
	var data DelData
	if err := context.Bind(&data); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10303,
			"msg":  "delete session fail," + err.Error(),
		})
		return
	}

	_, err := model.DelSession(data.OwnerId, data.OtherId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10303,
			"msg":  "delete session fail," + err.Error(),
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "delete session success",
	})
}

// CreateSession 创建会话列表
func CreateSession(context *gin.Context) {
	var s model.SessionList
	if err := context.Bind(&s); err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10303,
			"msg":  "create session fail," + err.Error(),
		})
		return
	}
	result, err := model.CreateSession(s)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10303,
			"msg":  "create session fail," + err.Error(),
		})
		return
	}
	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "create session success",
		"data": result.InsertedID,
	})
}

// GetOfflineMsg 获取离线消息
func GetOfflineMsg(context *gin.Context) {
	ownerId := context.Query("owner_id")
	otherId := context.Query("other_id")

	if ownerId == "" || otherId == "" {
		context.JSON(http.StatusOK, gin.H{
			"code": 10304,
			"msg":  "request params fail",
		})
		return
	}

	msgs, err := model.GetMsgList(otherId, ownerId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10305,
			"msg":  "get offline msg fail",
		})
		return
	}

	_, err = model.DelMsg(otherId, ownerId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10305,
			"msg":  "del offline msg fail",
		})
		return
	}

	_, err = model.ResetUnreadMsgCount(ownerId, otherId)
	if err != nil {
		context.JSON(http.StatusOK, gin.H{
			"code": 10306,
			"msg":  "reset unread msg count fail",
		})
		return
	}

	context.JSON(http.StatusOK, gin.H{
		"code": 0,
		"msg":  "reset unread msg count success",
		"data": msgs,
	})

}
