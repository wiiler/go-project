package conf

import (
	"fmt"

	"github.com/spf13/viper"
)

var (
	MysqlConf   MysqlIni
	RedisConf   RedisIni
	MongoDbConf MongoDbIni
)

type MysqlIni struct {
	Username string
	Password string
	Host     string
	Port     int
	Database string
}

type RedisIni struct {
	Host     string
	Port     string
	Password string
	Db       int
}

type MongoDbIni struct {
	Username string
	Password string
	Host     string
	Port     int
	Db       string
}

// viper 读取ini配置文件
func init() {
	config := viper.New()
	config.AddConfigPath("./application/conf/")
	config.SetConfigName("config")
	config.SetConfigType("ini")

	if err := config.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			fmt.Println("找不到配置文件..")
		} else {
			fmt.Println("配置文件出错..")
		}
		fmt.Println(err.Error())
		return
	}

	MysqlConf = MysqlIni{
		Username: config.GetString("mysql.username"),
		Password: config.GetString("mysql.password"),
		Host:     config.GetString("mysql.host"),
		Port:     config.GetInt("mysql.port"),
		Database: config.GetString("mysql.database"),
	}

	RedisConf = RedisIni{
		Host:     config.GetString("redis.host"),
		Port:     config.GetString("redis.port"),
		Password: config.GetString("redis.password"),
		Db:       config.GetInt("redis.db"),
	}

	MongoDbConf = MongoDbIni{
		Username: config.GetString("mongodb.username"),
		Password: config.GetString("mongodb.password"),
		Host:     config.GetString("mongodb.host"),
		Port:     config.GetInt("mongodb.port"),
		Db:       config.GetString("mongodb.db"),
	}
}
