package model

import (
	"context"
	app "im-demo/application"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

const (
	// type 消息类型
	MSG_TYPE_HEART  = 1 // 心跳消息
	MSG_TYPE_SINGLE = 2 // 单聊消息
	MSG_TYPE_ROOM   = 3 // 群聊消息
	MSG_TYPE_ACK    = 4 // 应答消息
	Msg_TYPE_OTHER  = 4 // 其他业务消息

	// media 消息展示样式
	MSG_MEDIA_TEXT  = 1 // 文本
	MSG_MEDIA_IMAGE = 2 // 图片
	MSG_MEDIA_FILE  = 3 // 文件
)

type Message struct {
	MsgId      int64  `json:"msg_id,omitempty" form:"msg_id" bson:"msg_id"`       // 消息ID
	OwnerId    string `json:"owner_id,omitempty" form:"owner_id" bson:"owner_id"` // 己方，发出者
	OtherId    string `json:"other_id,omitempty" form:"other_id" bson:"other_id"` // 对方，接收者
	MsgType    int    `json:"msg_type,omitempty" form:"msg_type" bson:"msg_type"` // 消息类型
	Media      int    `json:"media,omitempty" form:"media" bson:"media"`          // 消息按什么样式展示
	Content    string `json:"content,omitempty" form:"content" bson:"content"`    // 消息内容
	Pic        string `json:"pic,omitempty" form:"pic" bson:"pic"`                // 预览图片
	Url        string `json:"url,omitempty" form:"url" bson:"url"`                // 图片地址
	Desc       string `json:"desc,omitempty" form:"desc" bson:"desc"`             // 描述内容
	CreateTime int64  `json:"create_time" form:"create_time" bson:"create_time"`  // 消息时间
}

// CreateMsg 新增离线消息
func CreateMsg(msg *Message) (interface{}, error) {
	return app.MongoDBClient.Database(app.MongoDataBaseName).Collection(app.MongoMsgCollection).InsertOne(context.TODO(), msg)
}

// GetMsgList 获取消息
func GetMsgList(OwnerId, OtherId string) ([]*Message, error) {
	var results []*Message
	filter := bson.M{
		"owner_id": OwnerId,
		"other_id": OtherId,
	}
	findOptions := options.Find()
	findOptions.SetSort(bson.M{"msg_id": 1}) // msg_id升序

	cur, err := app.MongoDBClient.Database(app.MongoDataBaseName).Collection(app.MongoMsgCollection).Find(context.TODO(), filter, findOptions)
	if err != nil {
		return nil, err
	}
	defer cur.Close(context.TODO())

	for cur.Next(context.TODO()) {
		// 创建一个值，将单个文档解码为该值
		var elem Message
		err := cur.Decode(&elem)
		if err != nil {
			return nil, err
		}
		results = append(results, &elem)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	return results, nil
}

// DelMsg 删除消息
func DelMsg(OwnerId, OtherId string) (*mongo.DeleteResult, error) {
	filter := bson.M{
		"owner_id": OwnerId,
		"other_id": OtherId,
	}
	return app.MongoDBClient.Database(app.MongoDataBaseName).Collection(app.MongoMsgCollection).DeleteMany(context.TODO(), filter)
}
