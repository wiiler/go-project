module im-demo

go 1.16

require (
	github.com/bwmarrin/snowflake v0.3.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/websocket v1.5.0
	github.com/jinzhu/gorm v1.9.16
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/spf13/viper v1.10.1
	go.mongodb.org/mongo-driver v1.9.0
)
